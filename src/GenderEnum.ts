enum GenderEnum {
    Unknown = 0,
    Male = 1,
    Female = 2,
    NotApplicable = 9
}

export {
    GenderEnum
}