export class VatValidator {
  public static validate(value) {
    // Skip validation if value is empty
    if (!value) {
      return true;
    }

    return this.isValidVat(value);
  }

  private static isValidVat(vatNumber: string): boolean {
    // Check if vatNumber is formed by 9 digits
    if (!/^\d{9}$/.test(vatNumber)) {
      return false;
    }

    // Check if vatNumber is formed only by zeros
    if (vatNumber === '000000000') {
      return false;
    }

    const sum: number = vatNumber
      .substring(0, 8)
      .split('')
      .reduce((s, v, i) => s + (parseInt(v, 10) * Math.pow(2, 8 - i)), 0);

    const remainder: number = sum % 11;
    const lastDigit: number = parseInt(vatNumber.charAt(8), 10);

    return (remainder === 10 && lastDigit === 0) || lastDigit === remainder;
  }
}
