import { GrammarLocale } from './GrammarLocale';
import { GenderEnum } from './GenderEnum';
import * as locales from './locales';

declare interface AccusativeFormatterOptions {
    gender: GenderEnum
}

class AccusativeFormatter {
    static format(value: string, locale: string, options: AccusativeFormatterOptions): string {
        if (value == null) {
            return value;
        }
        if (Object.prototype.hasOwnProperty.call(locales, locale) === false) {
            return value;
        }
        const currentLocale: GrammarLocale = locales[locale];
        let replacements: string[][] = [];
        if (options.gender === GenderEnum.Male && currentLocale.accusative.Male != null) {
            replacements = currentLocale.accusative.Male.replacements;
        } else if (options.gender === GenderEnum.Female && currentLocale.accusative.Female != null) {
            replacements = currentLocale.accusative.Female.replacements;
        }
        if (replacements.length === 0) {
            return value;
        }
        for(const replacement of replacements) {
            const re = new RegExp(replacement[0], 'g');
            if (re.test(value)) {
                return value.replace(re, replacement[1]);
            }
        }
        return value;
    }
    static registerLocale(locale: string, options: GrammarLocale) {
        Object.defineProperty(locales, locale, {
            configurable: true,
            enumerable: true,
            writable: true,
            value: options
        });
    }
}

export {
    AccusativeFormatterOptions,
    AccusativeFormatter
}
