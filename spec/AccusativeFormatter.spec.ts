import {AccusativeFormatter, GenderEnum} from '../src/index';

describe('AccusativeFormatter', () => {

    it('should format greek male names', () => {
        const options = {
            gender: GenderEnum.Male
        };
        expect(AccusativeFormatter.format('ΘΕΣΣΑΛΟΝΙΚΕΥΣ', 'el', options)).toBe('ΘΕΣΣΑΛΟΝΙΚΕΑ');
    });

    it('should format greek female names', () => {
        const options = {
            gender: GenderEnum.Female
        };
        expect(AccusativeFormatter.format('ΠΑΠΑΔΟΠΟΥΛΟΥ', 'el', options)).toBe('ΠΑΠΑΔΟΠΟΥΛΟΥ');

    });

});
