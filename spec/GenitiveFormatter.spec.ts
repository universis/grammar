import {GenitiveFormatter, GenderEnum} from '../src/index';

describe('GenitiveFormatter', () => {

    it('should format greek male names', () => {
        const options = {
            gender: GenderEnum.Male
        };
        expect(GenitiveFormatter.format('ΓΕΩΡΓΙΟΣ', 'el', options)).toBe('ΓΕΩΡΓΙΟΥ');
        expect(GenitiveFormatter.format('Γεώργιος', 'el', options)).toBe('Γεώργιου');
    });

    it('should format greek female names', () => {
        const options = {
            gender: GenderEnum.Female
        };
        expect(GenitiveFormatter.format('ΓΕΩΡΓΙΑ', 'el', options)).toBe('ΓΕΩΡΓΙΑΣ');
        expect(GenitiveFormatter.format('Γεωργία', 'el', options)).toBe('Γεωργίας');
        expect(GenitiveFormatter.format('Φρόσω', 'el', options)).toBe('Φρόσως');
        expect(GenitiveFormatter.format('Δηδώ', 'el', options)).toBe('Δηδώς');
    });

});